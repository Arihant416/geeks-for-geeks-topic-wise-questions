#include<bits/stdc++.h>
using namespace std;
struct Node{
    int val;
    Node* left;
    Node* right;
};
// Function to create a Node
Node* createNode(int data){
    Node* newNode = new Node();
    if(!newNode){
        cout << "Memory Error!";
        return NULL;
    }
    newNode->val = data;
    newNode->left=newNode->right=NULL;
    return newNode;
}

// Function to insert a Node in a tree -> level order insertion is followed
Node* insertNode(int data,Node* root){
    if(root==NULL){
        root = createNode(data);
        return root;
    }
    queue<Node*>q;
    q.push(root);
    while(!q.empty()){
        Node* temp=q.front();
        q.pop();
        if(temp->left == NULL){
            temp->left=createNode(data);
            return root;
        }
        else{
            q.push(temp->left);
        }
        if(temp->right == NULL){
            temp->right = createNode(data);
            return root;
        }
        else{
            q.push(temp->right);
        }
    }
    return NULL;
}
void inorderTraversal(Node* root){
    if(!root)return;
    inorderTraversal(root->left);
    cout << root->val << " ";
    inorderTraversal(root->right);
}
int main(){
    Node* root = createNode(10);
    root->left = createNode(11);
    root->left->left = createNode(7);
    root->right = createNode(9);
    root->right->left = createNode(14);
    root->right->right = createNode(18);

    cout << "Inorder traversal before insertion\n";
    inorderTraversal(root);
    cout <<"\n";
    cout << "Inorder Traversal after Insertion\n";
    insertNode(200,root);
    inorderTraversal(root);

    return 0;
}