#include<bits/stdc++.h>
using namespace std;
// Creating a tree
struct Node{
    int data;
    struct Node *left,*right;
};
// Insert a new Node
struct Node* newNode(int key){
    struct Node* temp = new Node;
    temp->data = key;
    temp->left = temp->right = NULL;
    return temp;
}

void inorderTraversal(struct Node* root){
    if(!root){
        return;
    }
    inorderTraversal(root->left);
    cout << root->data <<" ";
    inorderTraversal(root->right);
}

void deleteDeep(struct Node* root, struct Node* delete_node){
    queue<Node*>q;
    q.push(root);
    struct Node* temp;
    while(!q.empty()){
        temp = q.front();
        q.pop();
        if(temp == delete_node){
            temp = NULL;
            delete (delete_node);
            return;
        }
        if(temp->right){
            if(temp->right==delete_node){
                temp->right=NULL;
                delete (delete_node);
                return ;
            }
            else q.push(temp->right);
        }
        if(temp->left){
            if(temp->left==delete_node){
                temp->left=NULL;
                delete delete_node;
                return;
            }
            else q.push(temp->left);
        }
    }
}

Node* deletion(struct Node* root, int key){
    if(root==NULL)return NULL;
    if(root->left == NULL && root->right == NULL){
        if(root->data == key)return NULL;
        else return root;
    }
    queue<struct Node*> q;
    q.push(root);
    struct Node* temp;
    struct Node* key_node=NULL;
    while(!q.empty()){
        temp  = q.front();
        q.pop();
        if(temp->data == key){
            key_node=temp;
        }
        if(temp->left){
            q.push(temp->left);
        }
        if(temp->right){
            q.push(temp->right);
        }
    }
    if(key_node!=NULL){
        int x = temp->data;
        deleteDeep(root,temp);
        key_node->data = x;
    }
    return root;
}

int main(){
    struct Node* root = newNode(10);
    root->left = newNode(11);
    root->right = newNode(12);
    root->left->left = newNode(15);
    root->right->left= newNode(16);
    cout<<"Before Deletion\n";
    inorderTraversal(root);
    cout<<"\n";
    cout<<"After Deletion\n";
    deletion(root,12);
    inorderTraversal(root);
    return 0;
}