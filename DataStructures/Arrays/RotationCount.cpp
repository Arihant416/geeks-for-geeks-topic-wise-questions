#include<bits/stdc++.h>
using namespace std;
class Solution{
public:	
	int findKRotation(int a[], int n) {
	    int low=0,high=n-1;
	    while(low<=high){
	        int mid=(high+low)/2;
	        if(mid<high && a[mid]>a[mid+1])return mid+1;
	        if(mid>low && a[mid]<a[mid-1])return mid;
	        if(a[mid]<=a[high])high=mid-1;
	        else low=mid+1;
	    }
	    
	}

};
int main(){
    int t;
    cin>>t;
    while(t--){
        int n;cin>>n;
        int a[n];for(int i=0;i<n;i++){
            cin>>a[i];
        }
        Solution object;
        auto ans=object.findKRotation(a,n);
        cout<<ans<<endl;
    }
}