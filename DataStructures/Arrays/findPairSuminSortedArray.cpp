#include<bits/stdc++.h>
using namespace std;
int findCount(vector<int>& a,int n,int sum){
    int i,low,right;
    for(i=0;i<n-1;i++){
        if(a[i]>a[i+1])break;
    }
    low=(i+1)%n;
    right=i;
    int count(0);
    while(low!=right){
        if(a[low]+a[right]==sum){
            count++;
            if(low==(right+n-1)%n)return count;
            low=(low+1)%n;
            right=(right+n-1)%n;
        }
        else if(a[low]+a[right]<sum){
            low=(low+1)%n;
        }
        else{
            right=(right+n-1)%n;
        }
    }
    return count;
}
int main(){
    vector<int>a={5,6,7,8,9,10,1,2,3,4};
    cout<<findCount(a,10,10)<<endl;
}