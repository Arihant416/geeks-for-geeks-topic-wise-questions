#include<bits/stdc++.h>
using namespace std;
int findElement(vector<int>& a,int low,int high,int target){
    //Base case
    if(high<low)return -1;
    int mid=(low+(high-low)/2);
    if(a[mid]==target)return mid;
    //Search for rotation if left part of array is sorted
    else if(a[mid]>=a[low]){
        if(a[mid]>=target && a[low]<=target)return findElement(a,low,mid+1,target);
        else return findElement(a,mid,high,target);
    }
    else if(a[mid]<=a[high]){
        if(a[mid]<=target && a[high]>=target)return findElement(a,mid,high,target);
        else return findElement(a,low,mid,target);
    }
}
int main(){
    int n=10;
    vector<int>a={5,6,7,8,9,10,1,2,3,4};
    cout<<findElement(a,0,9,10);
    return 0;
}