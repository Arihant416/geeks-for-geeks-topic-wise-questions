#include<bits/stdc++.h>
using namespace std;
long long int findMaxSum(vector<int>& a,int n){
    long long int sum=0,arrSum(0);
    for(int i=0;i<n;i++){
        sum+=(a[i]*i);
        arrSum+=a[i];
    }
    long long maxVal=sum;
    for(int i=1;i<n;i++){
        sum=sum+arrSum-n*a[n-i];
        if(sum>maxVal){
            maxVal=sum;
        }
    }
    return maxVal;
}
int main(){
    vector<int>a={1,20,2,10};
    cout<<findMaxSum(a,4)<<endl;
    return 0;
}