#include<bits/stdc++.h>
using namespace std;
int main(){
    int n=3;
    int a[n]={1,4,1};
    int b[2*n +1];
    for(int i=0;i<n;i++){
        b[i]=b[n+i]=a[i];
    }
    // for(int i=0;i<2*n ;i++){
    //     cout<<b[i]<<" ";
    // }
    int maximumHammingDistance(0);
    for(int i=1;i<n;i++){
        int currentHammingDistance=0,k(0);
        for(int j=i;j<(i+n);j++,k++){
            if(a[k]!=b[j])currentHammingDistance++;
        }
        if(currentHammingDistance==n){
            maximumHammingDistance=n;
            break;
        }
        maximumHammingDistance=max(maximumHammingDistance,currentHammingDistance);
    }
    cout<<maximumHammingDistance;
}